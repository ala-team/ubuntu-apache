FROM alateam/ubuntu-zsh-tty
MAINTAINER alateam <team@ala-it.com>


ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get -y install apache2 libapache2-mod-php5 php5-mysql php5-gd php-pear php-apc php5-curl curl lynx-cur 


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN wget http://jaist.dl.sourceforge.net/project/phpmyadmin/phpMyAdmin/4.3.9/phpMyAdmin-4.3.9-all-languages.tar.gz -O - | tar -xz

RUN mv phpMyAdmin* /phpmyadmin
ADD phpmyadmin.config.inc.php /phpmyadmin/config.inc.php


ADD apache.conf /etc/apache2/sites-available/000-default.conf


RUN mkdir -p /htdocs 
ADD htdocs/ /htdocs
RUN chown -R www-data:www-data /htdocs

RUN mkdir /apache_logs
RUN chown -R www-data:www-data /apache_logs

ADD run.sh /run.sh

RUN chmod 755 /*.sh


EXPOSE 80 3000 
WORKDIR /htdocs 



CMD ["/run.sh"]

